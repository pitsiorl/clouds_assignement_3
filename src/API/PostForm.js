import React, {Component} from 'react'
import axios from 'axios'


class PostForm extends Component {

    constructor(props){
        super(props)

        this.state = {
            id: '',
            title: '',
            category: '',
            author: '',
            books:[]
        }
        
    }

    componentDidMount(){
        axios.get('https://fj6tk1yl04.execute-api.us-east-2.amazonaws.com/books')
        .then(response =>{
            this.setState({
                books: response.data.Items
            })
            console.log(response.data)
        })
    }


    handleChange = (e) =>{
        this.setState({
            [e.target.name]: e.target.value
        })
    }


    handleSubmit = (e) => {
        e.preventDefault()
        axios.put('https://fj6tk1yl04.execute-api.us-east-2.amazonaws.com/books', this.state)
        .then(response =>{
            console.log(response)
        }).then(()=>{
            axios.get(`https://fj6tk1yl04.execute-api.us-east-2.amazonaws.com/books`)           
               .then(response =>{
               this.setState({
                   books: response.data.Items
               })
               console.log(response)
             })  
        })
    }

    handleUpdate = (e) => {
        e.preventDefault()
        axios.put('https://fj6tk1yl04.execute-api.us-east-2.amazonaws.com/books', this.state)
        .then(response =>{
            console.log(response)
        }).then(()=>{
            axios.get(`https://fj6tk1yl04.execute-api.us-east-2.amazonaws.com/books`)           
               .then(response =>{
               this.setState({
                   books: response.data.Items
               })
               console.log(response)
             })  
        })
    }
    
    deleteBook(id, e){  
        e.preventDefault()
        axios.delete(`https://fj6tk1yl04.execute-api.us-east-2.amazonaws.com/books/${id}`)  
          .then(res => {  
            console.log(res);  
            console.log(res.data);  
        }).then(()=>{
         axios.get(`https://fj6tk1yl04.execute-api.us-east-2.amazonaws.com/books`)           
            .then(response =>{
            this.setState({
                books: response.data.Items
            })
            console.log(response)
          })  
        })
      }  

      

    render(){
        const{id,title,category,author,books} = this.state
        return(
            <div>
                <div><h1>Books</h1></div>

                <form>
                    <div>
                        <label> Book ID </label>
                        <input 
                        type='text'
                        name = 'id'
                        value = {id}
                        onChange ={this.handleChange}
                        ></input>
                    </div>
                    <div>
                        <label> Title </label>
                        <input 
                        type='text'
                        name = 'title'
                        value = {title}
                        onChange ={this.handleChange}
                        ></input>
                    </div>
                    <div>
                        <label> Category </label>
                        <input 
                        type='text'
                        name = 'category'
                        value = {category}
                        onChange ={this.handleChange}
                        ></input>
                    </div> 
                    <div>
                        <label> Author </label>
                        <input 
                        type='text'
                        name = 'author'
                        value = {author}
                        onChange ={this.handleChange}
                        ></input>
                    </div> 

                    <div>
                    <button onClick={(e) =>(this.handleSubmit(e))}>Insert</button>
                    </div> 
                    <div>
                    <button onClick={(e) =>(this.handleUpdate(e))}>Update</button>
                    </div> 
                    <div>
                        <button onClick={(e) =>(this.deleteBook(id,e))}>Delete</button>
                    </div>

                </form>
                <div><h2>ID - Title - Category - Author</h2></div>
                <div>
                    {   
                        books.map(book => <div key={book.id}> {book.id} {book.title} {book.category} {book.author} </div>)
                    }
                </div>

            </div>
        )
    }
}

export default PostForm