import React , {Component} from 'react'
import axios from 'axios'

class PostList extends Component {

    constructor(props){
        super(props)

        this.state = {
            posts: []
        }

    }


    componentDidMount(){
        axios.get('https://fj6tk1yl04.execute-api.us-east-2.amazonaws.com/books')
        .then(response =>{
            this.setState({
                posts: response.data
            })
            console.log(response.data)
        })
    }



    render(){
        const {posts} = this.state
        return(
            <div>
                <h1>List of Books</h1>
                {                    
                    posts.map(book => <div key={book.id}>{book.title}</div>)
                }               
            </div>
        )
    }

}
export default PostList