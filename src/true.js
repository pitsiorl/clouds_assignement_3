import logo from './logo.svg';
import './App.css';
import React from 'react';

function App() {
  const [data, setData] = React.useState([]);
  const [name, setName] = React.useState("");
  React.useEffect(() => {
    const options = {
      'Method' : "GET",
      "Content-Type": "application/json"
    };
    const res = fetch("https://fj6tk1yl04.execute-api.us-east-2.amazonaws.com/books", options)
    .then(r =>  r.json())
    .then(r => setData(r.Items));
  }, [])

  const put = () => {
    const book = {
      name: name,
      
    };
    const options = {
      'Method' : "PUT",
      "Content-Type":"application/json",
      body: JSON.stringify(book)
    };
    const res = fetch("https://fj6tk1yl04.execute-api.us-east-2.amazonaws.com/books", options)
    setData([...data, book])
  }

  const update = () => {
    console.log('UPDATE');
  }

  const delete_con = () => {
    console.log('DELETE');
  }

      return (
          <div className="container">
            <div className="row justify-content-center">
              <div className="col-md-8">
                <h1 className="display-4 text-center">Books</h1>
                {data.map(b => (<p key={`${b.id}`}>{b.title}</p>))}
                <form className="d-flex flex-column">
                  <legend className="text-center">Add-Update-Delete Book</legend>
                  <label htmlFor="id">
                    Book id:
                    <input
                      name ="id"
                      id="id"
                      type="text"
                      className="form-control"
                      value={""}
                      onChange={(e) => setName(e.target.value)}
                      required
                      />
                  </label>
                  <label htmlFor="id">
                    Book title:
                    <input
                      name="title"
                      id="title"
                      type="text"
                      className="form-control"
                      value={""}
                      onChange={(e) => this.handleChange({ id: e.target.value })}
                      />
                  </label>
                  <label htmlFor="author">
                    Book author:
                    <input
                      name="author"
                      id="author"
                      type="test"
                      className="form-control"
                      value={""}
                      onChange={(e) => this.handleChange({ notes: e.target.value })}
                      required
                      />
                  </label>
                  <label htmlFor="category">
                    Book category:
                    <input
                      name="category"
                      id="category"
                      type="text"
                      className="form-control"
                      value={""}
                      onChange={(e) => this.handleChange({ id: e.target.value })}
                      />
                  </label>
                  <button className="btn btn-primary" type='button' onClick={put}>
                      Add
                  </button>
                  <button className="btn btn-info" type='button' onClick={update}>
                      Update
                  </button>
                  <button className="btn btn-danger" type='button' onClick={delete_con}>
                      Delete
                  </button>
                </form>
              </div>
            </div>
          </div>
      );
  }

export default App;
